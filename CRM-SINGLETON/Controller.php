<?php
include 'db.php';

$type = $_POST['type'];

switch ($type) {
    case 'save':
        echo save();
        break;
    case 'eliminar':
        echo delete();
        break;
    case 'getBook':
        echo getBooks();
        break;
    default:break;
}

function save(){
    $db = Database::getInstance();//Se obtiene o crea la instancia segun el estado
    $data = 0;
    $id = $_POST['id'];
    $nombre = $_POST['nombre'];
    $autor = $_POST['autor'];
    $isbn = $_POST['isbn'];
    if($id==""){
        $data = 1;
        $sql = "INSERT INTO libros(nombre,autor,isbn) VALUES('$nombre','$autor','$isbn')";
    }else{
        $data = 2;
        $sql = "UPDATE libros SET nombre = '$nombre', autor = '$autor', isbn = '$isbn' WHERE id = '$id'";
    }

    $result = $db->executeQuery($sql);

    if($result){
        return $data;
    }else{
        return 0;
    }
}

function delete(){
    $db = Database::getInstance();
    $id = $_POST['id'];
    $sql = "DELETE FROM libros WHERE id = '$id'";
    if($db->executeQuery($sql)){
        return 1;
    }else{
        return 0;
    }
    
}

function getBooks(){
    $db = Database::getInstance();
    $html = "";
    $sql = "SELECT * FROM libros";
    $data1 = $db->executeQuery($sql);
    if(mysqli_num_rows($data1)!=0){
        while ($data = mysqli_fetch_array($data1)){ 
           $html = $html.'<tr>
                <th>'.$data['id'].'</th>
                <td>'.$data['nombre'].'</td>
                <td>'.$data['autor'].'</td>
                <td>'.$data['isbn'].'</td>
                <td><button type="button" onclick="modal(\'Editar '.$data['nombre'].'\',\''.$data['nombre'].'\',\''.$data['autor'].'\',\''.$data['isbn'].'\',\''.$data['id'].'\')" class="btn btn-outline-warning">Editar</button></td>
                <td><button type="button" onclick="eliminar(\''.$data['id'].'\')" class="btn btn-outline-danger">Eliminar</button></td>
            </tr>';
        }
    }else{
        $html =  '<center><td class="col" colspan="6">No hay libros registrados.</td></center>';
    }

    return $html;
}
?>