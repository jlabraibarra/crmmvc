<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <script type="text/javascript" src="assets/jquery-3.4.0.min.js"></script>
    <script type="text/javascript" src="assets/notify.min.js"></script>
    <script type="text/javascript" src="assets/notify.js"></script>
    
    <title>Liberia</title>
</head>
<body>
    <div class="row md-12">
        <div class="col-lg-8 col" style="float: none;margin: 3% auto; border-style: outset;border-width: 2px;padding:20px;">
            <div class="row">
            <div class="col-8">
                <h1 class="display-4">Libreria de libros</h1>
            </div>
            <div class="col-4">
                <button type="button" class="btn btn-info btn-lg btn-block" onclick="modal('Agregar Libro','','','','')">Agregar libro</button>
            </div>
            </div>
            <div class="row-12">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Autor</th>
                            <th scope="col">ISBN</th>
                            <th scope="col">Editar</th>
                            <th scope="col">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody id="datosTabla">
                    </tbody>
                    <tfoot>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Autor</th>
                            <th scope="col">ISBN</th>
                            <th scope="col">Editar</th>
                            <th scope="col">Eliminar</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>

<div id="modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="titulo" class="modal-title"></h5>
                <button type="button" class="close" onclick="modalClose()" data-dimiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="modalReg">
                    <input type="text" class="form-control" id="id" hidden>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre del libro:</label>
                        <input type="text" class="form-control" id="nombre" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Autor:</label>
                        <input type="text" class="form-control" id="autor"  required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">ISBN:</label>
                        <input type="text" class="form-control" id="isbn">
                    </div>
                    <button type="button" onclick="return save()"  class="btn btn-primary">Registro</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    getBooks();

    function modal(title,nombre,autor,isbn,id){
        $('#titulo').html(title);
        $('#id').val(id);
        $('#nombre').val(nombre);
        $('#autor').val(autor);
        $('#isbn').val(isbn);

        $('#modal').show();
    }

    function save(){
        var id = $('#id').val();
        var nombre = $('#nombre').val();
        var autor = $('#autor').val();
        var isbn = $('#isbn').val();

        $.ajax({
            type:"POST", 
            url:"Controller.php",
            data:{
                type:'save',
                id:id,
                nombre:nombre,
                autor:autor,
                isbn:isbn
            }, 
            success:function(data){
                if(data == 1){
                    modalClose();
                    $.notify("Libro guardado",'success');
                    getBooks();
                }
                if(data == 2){
                    modalClose();
                    $.notify("Libro editado",'success');
                    getBooks();
                }
                if(data == 0){
                    $.notify("Fallo [DB]",'error');
                }
            }
        });
    }

    function getBooks(){
        $.ajax({
            type:"POST", 
            url:"Controller.php",
            data:{
                type:'getBook'
            }, 
            success:function(data){
                $('#datosTabla').html(data);
            }
        });
    }

    function eliminar(id){
        $.ajax({
            type:"POST", 
            url:"Controller.php",
            data:{
                type:'eliminar',
                id:id
            }, 
            success:function(data){
                if(data == 0){
                    $.notify("Fallo [DB]",'error');
                }else{
                    $.notify("Libro eliminado",'success');
                    getBooks();
                }
            }
        });
    }
    
    function modalClose(){
        $('#modal').hide();
    }
</script>
</html>