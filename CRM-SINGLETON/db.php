<?php

class Database {
    static private $instance = null;
    static private $server = 'mysql.webcindario.com';
    static private $nameBD = 'demopdv';
    static private $user = 'demopdv';
    static private $pass = 'gabriela_281195';

    private function __contruct() {}
//Singleton, creacion de una solo intancia, para ahorrar memoria.
    public static function getInstance() {
        if (self::$instance == null) {//verificacion de que existe una instancia.
            self::$instance = new Database();
        }
        return self::$instance;
    }

    public function connect() {
        $conexion = mysqli_connect( self::$server, self::$user, self::$pass, self::$nameBD) or die("Error al conectar");
        return $conexion;
    }

    public function executeQuery($sql) {
        $conn = $this->connect();
        $result = mysqli_query($conn,$sql);
        return $result;
    }

    public function disconnect($conexion) {
        mysqli_close($conexion);
    }
}
?>