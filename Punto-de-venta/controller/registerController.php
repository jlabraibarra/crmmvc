<?php
require_once "../model/LoginModel.php";

$name = $_POST['name'];
$email = $_POST['email'];
$pass1 = $_POST['pass1'];
$pass2 = $_POST['pass2'];

if ($name != "" || $email!= "" || $pass1!= "" || $pass2!= "") {
    if ($pass1 == $pass2) {
        if (!checkEmail($email)) {
            $pass = md5($pass1);
            if (saveUser($email,$pass,$name)) {
                echo 'success';
            }else{
                echo 'El usuario no pudo ser registrado [Error de BD]';
            }
        }else{
            echo 'Este correo ya esta registrado';
        }
    }else{
        echo 'Las contraseñas no coinciden';
    }
}else{
    echo 'Hay datos faltantes';
}