<?php
session_start();

if($_SESSION["sesion"] == false){
    echo "alert('No tienes una sesion iniciada.')";
    header('Location: index.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/style.css">
    <script type="text/javascript" src="assets/js/jquery-3.4.0.min.js"></script>
    <script type="text/javascript" src="assets/js/notify.min.js"></script>
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <title>Login | Home</title>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="200">
    <nav class="navbar navbar-expand-sm bg-light navbar-light fixed-top">
        <a class="navbar-brand" href="#">
            <img src="assets/images/logo_default.png" alt="Logo" style="width:100px;">
        </a>
        <ul class="navbar-nav float-right">
            <li class="nav-item active">
                <a class="nav-link" href="#">Active</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
        </ul>
    </nav>

    <div class="container" style="margin-top:5%;">
        <div class="card" style="width:300px;float: none;margin: 10% auto; border-style: outset;border-width: 2px;padding:20px;">
            <center>
                <img src="assets/images/user_default.png" width="150px">
                <div class="card-body">
                    <h4 class="card-title"><?php echo $_SESSION['name']; ?></h4>
                    <p class="card-text">Administrador del sitio.</p>
                </div>
            </center>
        </div>
    </div>
    
</body>