<?php
    session_start();

    if(isset($_SESSION["sesion"])){
        if($_SESSION["sesion"] == true){
            echo "alert('No tienes una sesion iniciada.')";
            header('Location: home.php');
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/style.css">
    <script type="text/javascript" src="assets/js/jquery-3.4.0.min.js"></script>
    <script type="text/javascript" src="assets/js/notify.min.js"></script>
    <script type="text/javascript" src="assets/js/notify.js"></script>
    <title>PVB | Login</title>
</head>
<body>
    <div class="row-12">
        <div class="col-4" style="float: none;margin: 10% auto; border-style: outset;border-width: 2px;padding:20px;">
            <center><img src="assets/images/logo_default.png" width="250px"></center>
            <hr>
            <form class="needs-validation" novalidate>
                <div class="form-group">
                    <label for="exampleInputEmail1">Correo electronico</label>
                    <input type="email" class="form-control" id="email" placeholder="Escriba su correo" >
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" class="form-control" id="password" placeholder="Escriba su contraseña" >
                </div>
                <button type="button" onclick="return login()"  class="btn btn-primary">Entrar</button>
                <button type="button" onclick="modalOpen()"  class="btn btn-primary">Registro</button>
            </form>
        </div>
    </div>
</body>

<div id="modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registro de nuevo usuario</h5>
                <button type="button" class="close" onclick="modalClose()" data-dimiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="modalReg">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre completo:</label>
                        <input type="text" class="form-control" id="nameR" placeholder="Escriba su nombre" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Correo electronico:</label>
                        <input type="email" class="form-control" id="emailR" placeholder="Escriba su correo" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Contraseña:</label>
                        <input type="password" class="form-control" id="passwordR" placeholder="Escriba su contraseña" >
                        <div class="valid-feedback">Correcto!</div>
                        <div class="valid-feedback">Este campo no debe estar vacio.</div>
                    </div><div class="form-group">
                        <label for="exampleInputPassword1">Repita su contraseña:</label>
                        <input type="password" class="form-control" id="passwordR2" placeholder="Escriba su contraseña" >
                        <div class="valid-feedback">Correcto!</div>
                        <div class="valid-feedback">Este campo no debe estar vacio.</div>
                    </div>
                    <button type="button" onclick="return registro()"  class="btn btn-primary">Registro</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function login() {
        var user = $('#email').val();
        var pass = $('#password').val();

        $.ajax({
            type:"POST", 
            url:"controller/loginController.php",
            data:{
                user:user,
                pass:pass
            }, 
            success:function(data){
                if(data == "success"){
                    $.notify("Credenciales correctas entrando...",'success');
                    setTimeout ("window.location='home.php'", 2000);
                }else{
                    $.notify(data,'error');
                }
            }
        });
    }

    function registro(){
        var name = $('#nameR').val();
        var email = $('#emailR').val();
        var pass1 = $('#passwordR').val();
        var pass2 = $('#passwordR2').val();

        $.ajax({
            type:"POST", 
            url:"controller/registerController.php",
            data:{
                name:name,
                email:email,
                pass1:pass1,
                pass2:pass2
            }, 
            success:function(data){
                if(data == "success"){
                    $.notify("Registro exitoso, inicia sesion.",'success');
                    $('#nameR').val("");
                    $('#emailR').val("");
                    $('#passwordR').val("");
                    $('#passwordR2').val("");
                    modalClose();
                }else{
                    $.notify(data,'error');
                }
            }
        });        
    }

    function modalOpen(){
        $('#modal').show();
    }
    function modalClose(){
        $('#modal').hide();
    }
</script>
</html>